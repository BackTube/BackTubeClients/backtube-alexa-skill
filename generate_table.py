from Modules.DaoImpl.UserDao import User

def create_table():
    userDao = User()
    created = userDao.create_table()
    if created:
        print "Table Creation complete"
    else:
        print "Some error occured!"

if __name__ == "__main__":
    create_table()
