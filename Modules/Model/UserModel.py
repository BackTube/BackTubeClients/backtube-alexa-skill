from pynamodb.models import Model
from pynamodb.attributes import UnicodeAttribute, NumberAttribute

class UserModel(Model):

    """
    User table that consists of googleId, nextPageToken, prevPageToken
    """

    class Meta:
        table_name = "User"
        region = "ap-south-1"

    userId = UnicodeAttribute(hash_key=True)
    searchQuery = UnicodeAttribute()
    nextPageToken = UnicodeAttribute()
    prevPageToken = UnicodeAttribute()
    accessToken = UnicodeAttribute()
    currentStream = UnicodeAttribute()
    currentStreamOffset = NumberAttribute()
