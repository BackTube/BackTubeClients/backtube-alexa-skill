from json import dumps

class YoutubeAudio:

    def __init__(self, audio_title, thumbnail_url, stream_url):
        self._audio_title = audio_title
        self._thumbnail_url = thumbnail_url
        self._stream_url = stream_url

    def get_audio_title(self):
        return self._audio_title

    def get_thumbnail_url(self):
        return self._thumbnail_url

    def get_stream_url(self):
        return self._stream_url

    def __str__(self):
        info_dict = {
        "title" : self.get_audio_title(),
        "thumbnail_url" : self.get_thumbnail_url(),
        "stream_url" : self.get_stream_url()
        }
        return dumps(info_dict)
