from boto3 import client
from Modules.Model.UserModel import UserModel

class User:

    def __init__(self):
        pass

    def create_table(self):
        try:
            UserModel.create_table(read_capacity_units = 1, write_capacity_units = 1)
            return True
        except Exception as e:
            print str(e)
            return False

    def insert_user(self, userId, nextPageTokenUser, prevPageTokenUser,
                        searchQueryUser, accessTokenUser):
        try:
            user = UserModel(userId, nextPageToken = nextPageTokenUser,
                            prevPageToken = prevPageTokenUser, searchQuery = searchQueryUser,
                            accessToken = accessTokenUser, currentStream = "None",
                            currentStreamOffset = 0)
            user.save()
            return True
        except Exception as e:
            print str(e)
            return False

    def update_user(self, userId, nextPageTokenUser, prevPageTokenUser,
                    searchQueryUser, accessTokenUser, currentStreamUser):
        try:
            user = UserModel.get(userId)
            user.nextPageToken = nextPageTokenUser
            user.prevPageToken = prevPageTokenUser
            user.searchQuery = searchQueryUser
            user.accessToken = accessTokenUser
            user.currentStream = currentStreamUser
            user.currentStreamOffset = 0
            user.save()
            return True
        except Exception as e:
            print str(e)
            return False

    def update_offset_user(self, userId, offSet):
        try:
            user = UserModel.get(userId)
            user.currentStreamOffset = offSet
            user.save()
            return True
        except Exception as e:
            print str(e)
            return False

    def find_user(self, userId):
        try:
            user = UserModel.get(userId)
            return user
        except Exception as e:
            print str(e)
            return None
