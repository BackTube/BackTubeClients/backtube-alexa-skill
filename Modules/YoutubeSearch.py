from requests import get
from config import SEARCH_URL, END_POINT
from requests import get
from Model.YouTubeAudio import YoutubeAudio

class YoutubeSearch:

    def __init__(self, access_token):
            self.access_token = access_token

    def search_song(self, search_string, page_token):
        request_params = {'access_token' : self.access_token, 'type' : 'video',
                        'region' : 'IN', 'maxResults' : 1, 'part' : 'snippet',
                        'q' : search_string, 'pageToken' : page_token}
        if page_token == None or page_token == "None":
            request_params = {'access_token' : self.access_token, 'type' : 'video',
                            'region' : 'IN', 'maxResults' : 1, 'part' : 'snippet',
                            'q' : search_string, 'pageToken' : ''}
        response = get(SEARCH_URL, params=request_params).json()
        return response

    def extract_items_from_response(self, response):
        return response.get("items")[0]

    def get_video_id(self, item):
        audio_id = None
        audio_id = item.get("id").get("videoId")
        return audio_id

    def get_thumbnail_url(self, item):
        url = None
        url = item.get('snippet').get('thumbnails').get('high').get('url')
        return url

    def get_audio_title(self, item):
        title = None
        title = item.get('snippet').get('title')
        return title

    def get_page_tokens(self, response):
        try:
            next_page_token = response.get("nextPageToken")
            prev_page_token = response.get("prevPageToken", "None")
            return [prev_page_token, next_page_token]
        except Exception as e:
            print str(e)
            return None

    def get_stream_url(self, videoId):
        stream_url = get(END_POINT + str(videoId)).text
        return stream_url

    def get_song_meta(self, search_string, page_token):
        song_data = self.search_song(search_string, page_token)
        item = self.extract_items_from_response(song_data)
        video_id = self.get_video_id(item)
        page_tokens = self.get_page_tokens(song_data)
        youtube_audio = YoutubeAudio(self.get_audio_title(item),
                                    self.get_thumbnail_url(item),
                                    self.get_stream_url(video_id))
        return [youtube_audio, page_tokens]
