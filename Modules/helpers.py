from config import GOOGLE_USER_INFO_URL
from requests import get

def get_google_user_id(access_token):
    request_params = {'access_token' : access_token}
    response = get(GOOGLE_USER_INFO_URL, params = request_params).json()
    return response.get("id")
