from flask import Flask, render_template
from logging import getLogger
from flask_ask import Ask, question, statement, audio, session, context
from Modules.YoutubeSearch import YoutubeSearch
from Modules.DaoImpl.UserDao import User
import json
import logging

application = Flask(__name__)
ask = Ask(application, "/")

logging.getLogger('flask_ask').setLevel(logging.DEBUG)

logger = getLogger()

@ask.launch
def new_session():
    if not session.user.accessToken:
        return statement(render_template("link_account")).link_account_card()
    userDao = User()
    userId = session.user.userId
    userExist = userDao.find_user(str(userId))
    if userExist == None:
        userDao.insert_user(str(userId), "None", "None", "None", session.user.accessToken)
    else:
        userDao.update_user(str(userId), "None", "None","None", session.user.accessToken, "None")
    return question(render_template("welcome"))

@ask.intent("PlayIntent")
def play_song(anyName):
    try:
        logger.info("Requested song " + str(anyName))
        userDao = User()
        userId = session.user.userId
        user = userDao.find_user(userId)
        if user == None:
            return statement(render_template("link_account")).link_account_card()
        youtube_search = YoutubeSearch(session.user.accessToken)
        audio_meta = youtube_search.get_song_meta(anyName, None)
        youtube_audio = audio_meta[0]
        stream_url = youtube_audio.get_stream_url()
        userDao.update_user(str(userId), audio_meta[1][1], audio_meta[1][0], anyName, session.user.accessToken, stream_url)
        return audio("Powered by Backtube").play(stream_url).standard_card(title=youtube_audio.get_audio_title(),
                                                                            large_image_url=youtube_audio.get_thumbnail_url())
    except Exception as e:
        logger.error(str(e))
        return question(render_template("no_reply"))

@ask.intent('AMAZON.PauseIntent')
def pause():
    userId = context.System.user.userId
    offSet = context.AudioPlayer.offsetInMilliseconds
    userDao = User()
    userDao.update_offset_user(userId, offSet)
    return audio('').stop()

@ask.intent('AMAZON.NextIntent')
def next_song():
    userDao = User()
    userId = session.user.userId
    user = userDao.find_user(userId)
    youtube_search = YoutubeSearch(session.user.accessToken)
    audio_meta = youtube_search.get_song_meta(user.searchQuery, user.nextPageToken)
    youtube_audio = audio_meta[0]
    stream_url = youtube_audio.get_stream_url()
    userDao.update_user(str(userId), audio_meta[1][1], audio_meta[1][0], user.searchQuery, session.user.accessToken, stream_url)
    return audio('').play(stream_url).standard_card(title=youtube_audio.get_audio_title(),
                                                    large_image_url=youtube_audio.get_thumbnail_url())

@ask.intent('AMAZON.ResumeIntent')
def resume():
    userDao = User()
    userId = session.user.userId
    user = userDao.find_user(userId)
    if user.currentStream == "None":
        return question(render_template("no_reply"))
    return audio('').play(user.currentStream, offset = user.currentStreamOffset)

@ask.intent('AMAZON.StopIntent')
@ask.intent('AMAZON.CancelIntent')
def stop():
    return audio('').stop()

@ask.intent("AMAZON.PreviousIntent")
def prev_song():
    userDao = User()
    userId = session.user.userId
    user = userDao.find_user(userId)
    youtube_search = YoutubeSearch(session.user.accessToken)
    audio_meta = youtube_search.get_song_meta(user.searchQuery, user.prevPageToken)
    youtube_audio = audio_meta[0]
    stream_url = youtube_audio.get_stream_url()
    userDao.update_user(str(userId), audio_meta[1][1], audio_meta[1][0], user.searchQuery, session.user.accessToken, stream_url)
    return audio('').play(stream_url).standard_card(title=youtube_audio.get_audio_title(),
                                                    large_image_url=youtube_audio.get_thumbnail_url())

@ask.intent("SeekForwardIntent", convert={'offSet' : int})
def seek_forward(offSet):
    try:
        userDao = User()
        userId = session.user.userId
        current_stream_url = userDao.find_user(userId).currentStream
        current_offset = int(context.AudioPlayer.offsetInMilliseconds)
        new_offset = current_offset + ((offSet / 100) * current_offset)
        return audio('').play(current_stream_url, offset = new_offset)
    except Exception as e:
        logger.error(str(e))
        return question(render_template("error_string"))

@ask.intent("SeekBackwardIntent", convert={'offSet' : int})
def seek_forward(offSet):
    try:
        userDao = User()
        userId = session.user.userId
        current_stream_url = userDao.find_user(userId).currentStream
        current_offset = int(context.AudioPlayer.offsetInMilliseconds)
        new_offset = current_offset - ((offSet / 100) * current_offset)
        if new_offset < 0:
            new_offset = 0
        if current_stream_url != None:
            return audio('').play(current_stream_url, offset = new_offset)
    except Exception as e:
        logger.error(str(e))
        return question(render_template("error_string"))

@ask.intent("AMAZON.HelpIntent")
def help_intent():
    return question(render_template("help_text_template"))

@ask.on_playback_nearly_finished()
def play_back_finished():
    userDao = User()
    userId = context.System.user.userId
    accessToken = context.System.user.accessToken
    user = userDao.find_user(userId)
    youtube_search = YoutubeSearch(accessToken)
    audio_meta = youtube_search.get_song_meta(user.searchQuery, user.nextPageToken)
    youtube_audio = audio_meta[0]
    stream_url = youtube_audio.get_stream_url()
    userDao.update_user(str(userId), audio_meta[1][1], audio_meta[1][0], user.searchQuery, accessToken, stream_url)
    return audio('').play(stream_url)

@ask.on_playback_started()
def play_back_started(offset, token, url):
    logger.info("Playback started for ")
    return "{}", 200

@ask.on_playback_stopped()
def play_back_stopped(offset, token):
    logger.info("Playback stopped ")
    return "{}", 200

@ask.session_ended
def session_ended():
    return "{}", 200

if __name__ == "__main__":
    application.run(debug=True)
